Implementation Collusion Attack in OLSR <br>
Nama : Dino Budi P. <br>
NRP : 05111850010036 <br>
Mt.Kul : TD. Desain Audit Jaringan <br>
Implementasi dan mendeteksi serangan Collusion pada OLSR. Pada paper dijelaskan bahwa dalam mendeteksi serangan Collusion cukup susah, dikarenakan mengedrop paket data dan mengubah pesan pada routing yang dilewati.
Untuk mengatasinya, saya mengajukan beberapa metode untuk menanggulangi dalam serangan Collusion pada OLSR yaitu, menambahkan 3 control message tambahan diantaranya adalah 2-hop request, 2-hop reply dan Node Exist Query (NEQ).
NEQ nantinya akan digunakan dalam mendeteksi bahwa node yang telah terkena serangan akan langsung di drop.